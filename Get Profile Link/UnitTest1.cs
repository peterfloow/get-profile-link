﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Mime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace ScrapeData
{
    [TestClass]

    public class ScrapeData
    {
        private TestContext testContextInstance;
        private IWebDriver _driver;
        private string _proxyURL;
        private string _loginURL;
        private string _replaceLine;
        private string _currentLine;
        private string[] _personalEmail;

        [TestMethod]
        [TestCategory("Chrome")]

        public void GetProfileLink()
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            _driver.Navigate().GoToUrl(_loginURL);
            //_driver.Manage().Window.Maximize();

            Random random = new Random();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='username']"))).SendKeys("betapchat.000@gmail.com");
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='password']"))).SendKeys("superman123");
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='app__container']/main/div/form/div[3]/button"))).Click();

            Thread.Sleep(random.Next(1000, 2500));

            var text = File.ReadLines(@"D:\csvFile.csv").ToString();

            using (StreamReader sr = new StreamReader(@"D:\csvFile.csv"))
            {
                while ((_currentLine = sr.ReadLine()) != null)
                {
                    _driver.Navigate().GoToUrl(_proxyURL + _currentLine);

                    Thread.Sleep(random.Next(1000, 2000));

                    _replaceLine = _currentLine + "," + _driver.Url;
                    using (StreamWriter sw = new StreamWriter(@"D:\listFile.csv", true))
                    {
                        sw.WriteLine(_replaceLine);
                    }
                }
            }
        }

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void SetupTest()
        {
            _proxyURL = "https://www.linkedin.com/sales/gmail/profile/proxy/";
            _loginURL = "https://www.linkedin.com/login?trk=guest_homepage-basic_nav-header-signin";

            //Open Chrome in Incognito mode
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--incognito");

            var proxy = new Proxy();
            //Set the http proxy value, host and port.
            proxy.HttpProxy = "localhost:8888";
            //Set the proxy to the Chrome options
            options.Proxy = proxy;

            //_driver = new ChromeDriver(@"D:\AutoKaercher\AutoKaercher\bin\Debug", options);
            _driver = new ChromeDriver(options);

            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);

        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            _driver.Quit();
        }
    }
}